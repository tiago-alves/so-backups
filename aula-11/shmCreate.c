#include "defines.h"
/**
 * @brief Creates (exclusively) a shared memory defined by IPC_KEY
 * @param size size in Bytes of the shared memory
 * @return the corresponding shmId of the shared memory, or -1 in case of error
 */
int shmCreate (int size) 
{
    #define ACCESS_PERMS 0600
    int shmId = shmget(IPC_KEY, size, IPC_CREAT | IPC_EXCL | ACCESS_PERMS);
    
	so_exit_on_error(shmId, "Erro shmget Create");

        so_debug("Criei a memória partilhada com key=0x%x e id=%d", IPC_KEY, shmId);
    return shmId;
}
