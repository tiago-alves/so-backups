#ifndef __COMMON_H__
    #define __COMMON_H__

    #include "/home/so/utils/include/so_utils.h"
    #include <sys/ipc.h>
    #include <sys/sem.h>
    #include <sys/msg.h>
    #include <sys/shm.h>
    #include <unistd.h>

    #define MSGDATA_MAXSIZE 8192    // Maximum size of msgData on Tigre

    typedef struct {
        int numero;
        char nome[99];
        float nota;
    } Aluno;

    /**
     * Prototypes
     */
    int semCreate (int);
    int semGet ();
    int semRemove (int);
    int semSetValue (int, int, int);
    int semGetValue (int, int);
    int semUp (int, int);
    int semDown (int, int);
    int semAddValue (int, int, int);

    int shmCreate (int);
    int shmGet ();
    int shmRemove (int);
    Aluno *shmAttach (void);
    void shmInit (Aluno *, int);
    void shmView (Aluno *, int);
    Aluno obtemDadosAluno ();
    int registaAluno (Aluno *, int, Aluno);
#endif // __COMMON_H__
