#include "defines.h"
/**
 * @brief obtains from the user interface the data of an Aluno,
 *        validating each input and exiting if error.
 * @return a filled structure Aluno
 */
Aluno obtemDadosAluno () {
    Aluno a;

    printf("Indique o Número do aluno: ");
    a.numero = so_geti();
    if (!a.numero && errno)
        so_exit_on_error(-1, "Erro so_geti");

    printf("Indique o Nome do aluno: ");
    so_exit_on_null(so_gets(a.nome, 99), "Erro so_gets");

    printf("Indique a Nota do aluno: ");
    a.nota = so_getf();
    if (!a.nota && errno)
        so_exit_on_error(-1, "Erro so_getf");
 
    return a;   
}

/**
 * @brief registers a new Aluno in the array turma
 * @return the index of the array where the Aluno was inserted,
 *         or -1 if the array is full
 */
int registaAluno (Aluno *turma, int nrAlunos, Aluno a) {
    #define ENTRADA_VAZIA -1
    int i;
    for(i = 0; i < nrAlunos; ++i) {
        if (ENTRADA_VAZIA == turma[i].numero) {
            so_debug("A inserir o aluno na posição %d", i);
            turma[i] = a;
            break;
        }
    }
    return i < nrAlunos ? i : -1; // Registou aluno? retorna o índice, senão retorna -1
}