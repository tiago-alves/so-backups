#include "defines.h"

#define NR_ALUNOS 5


int main()
{
	int shmId = shmCreate( NR_ALUNOS * sizeof(Aluno));

	Aluno *turma = shmAttach();
	
	shmView(turma, NR_ALUNOS);
	shmInit(turma, NR_ALUNOS);
	shmView(turma, NR_ALUNOS);
	
	getchar();
}
