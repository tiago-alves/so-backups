#include "defines.h"

int main () {
    // 5.
    // Isto pode ser feito de duas maneiras:
    // - Ou o aluno copia a função shmCreate(), definida em shmCreate.c para aqui (antes do main) e compila apenas test-shm.c que resulta em test-shm.exe
    // - Ou o aluno simplesmente faz: so_compile.sh test-shm.c shmCreate.c, que resulta em test-shm.exe
    #define NR_ALUNOS 5
    int shmId = shmCreate(NR_ALUNOS * sizeof(Aluno));
    so_exit_on_error(shmId, "shmCreate");

    // Este getchar() é só para mostrar aos alunos que, no terminal da direita, nattach=1 enquanto estivermos dentro do programa
    // Para sair, basta fazer <ENTER>
    getchar();
}