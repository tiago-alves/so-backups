#include "defines.h"
/**
 * @brief Gets (retrieves) an already created shared memory defined by IPC_KEY
 * @return the corresponding shmId of the shared memory, or -1 in case of error
 */
int shmGet () {
    #define DONT_CARE 0
    #define IPC_GET 0
    int shmId = shmget(IPC_KEY, DONT_CARE, IPC_GET);
    if (shmId > 0) {
        so_debug("Estou a usar a memória partilhada com key=0x%x e id=%d", IPC_KEY, shmId);
    }
    return shmId;
}