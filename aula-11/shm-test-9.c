#include "defines.h"

#define NR_ALUNOS	5
#define IPC_GET		0
#define DONT_CARE	0

int main()
{
	
	int shmId = shmget(IPC_KEY, DONT_CARE, IPC_GET);

	Aluno *turma;
	
	if (shmId < 0)
	{

		shmId = shmCreate( NR_ALUNOS * sizeof(Aluno));
	
		turma = shmAttach();

		shmView(turma, NR_ALUNOS);
		shmInit(turma, NR_ALUNOS);
	}
	else
	{
		so_debug("Estou a usar a memoria partilhada com key=ox%x e id=%d", IPC_KEY, shmId);

		turma = shmAttach();
	}	
	
	while( TRUE )
	{
		Aluno a = obtemDadosAluno();
		if( registaAluno(turma, NR_ALUNOS, a) < 0 )
			break;

		shmView(turma, NR_ALUNOS);
	}
	getchar();
}
