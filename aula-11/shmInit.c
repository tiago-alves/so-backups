#include "defines.h"

#define ENTRADA_VAZIA -1

/**
 * @brief Initializes the contents of an array turma of Alunos
 * @param turma address of the first element Aluno of the turma
 * @param nrAlunos number of elements of the array turma
 */

void shmInit (Aluno *turma, int nrAlunos) 
{

    for (int i = 0; i < nrAlunos; ++i) 
	{
        turma[i].numero = ENTRADA_VAZIA;
        strncpy(turma[i].nome, "[sem nada]", sizeof(turma[i].nome));
        turma[i].nota = 0.0;
    }
}
