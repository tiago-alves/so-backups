#include "defines.h"
/**
 * @brief Shows the contents of an array turma of Alunos
 * @param turma address of the first element Aluno of the turma
 * @param nrAlunos number of elements of the array turma
 */
void shmView (Aluno *turma, int nrAlunos) {
    so_debug("Conteúdo da SHM:");
    for (int i = 0; i < nrAlunos; ++i) {
        so_debug("Posição %2d: %6d | %-29s | %2.3f", 
                i, turma[i].numero, turma[i].nome, turma[i].nota);
    }
}