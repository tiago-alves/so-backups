#include "defines.h"
/**
 * @brief Connects the shared memory defined by IPC_KEY to a local memory address buffer
 * @param shmId id of the shared memory defined by IPC_KEY
 * @return the pointer to an array of Aluno, or NULL if error
 */

#define DONT_CARE	0
#define IPC_GET		0

Aluno *shmAttach () 
{
    #define NEW_ADDRESS NULL
    #define NO_FLAGS 0
	
	int shmId = shmget(IPC_KEY, DONT_CARE, IPC_GET);
    Aluno *turma = (Aluno *) shmat(shmId, NEW_ADDRESS, NO_FLAGS);

    if (turma)
        so_debug("A Shared Memory foi Attached no endereço %p", turma);
    else
		so_debug("Erro shmat");
	
	return turma;
}
