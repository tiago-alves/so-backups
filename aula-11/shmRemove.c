#include "defines.h"
/**
 * @brief Removes the shared memory defined by IPC_KEY
 * @param shmId id of the shared memory defined by IPC_KEY
 * @return 0 in case of success, or -1 in case of error
 */
int shmRemove (int shmId) {
    #define DONT_CARE 0
    int status = shmctl(shmId, IPC_RMID, NULL);
    if (!status) {
        so_debug("Removi a memória partilhada com key=0x%x e msgId=%d", IPC_KEY, shmId);
    }
    return status;
}