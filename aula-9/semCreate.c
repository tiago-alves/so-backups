#include "defines.h"
/**
 * @brief Creates (exclusively) a semaphore array defined by IPC_KEY
 * @param nrSems number of semaphores in the semaphore array
 * @return the corresponding semId of the semaphore array, or -1 in case of error
 */
int semCreate (int nrSems) {
    #define ACCESS_PERMS 0600
    int semId = semget(IPC_KEY, nrSems, IPC_CREAT | IPC_EXCL | ACCESS_PERMS);
    if (semId > 0) {
        so_debug("Criei o grupo de semáforos com key=0x%x e semId=%d", IPC_KEY, semId);
    }
    return semId;
}