#include "defines.h"
/**
 * @brief Adds -1 to the value of the Semaphore idxSem (of the semaphore array defined by IPC_KEY)
 * @param semId id of the semaphore array defined by IPC_KEY
 * @param idxSem index of the semaphore in the semaphore array defined by IPC_KEY
 * @return the value of the semaphore after the operation, or -1 in case of error
 */
int semDown (int semId, int idxSem) {
	#define SEM_ADD_VALUE_MINUS_ONE -1
    so_debug("PID %d: ANTES da operação de adição de %d ao semáforo %d", getpid(), SEM_ADD_VALUE_MINUS_ONE, idxSem);
    struct sembuf operation = {idxSem, SEM_ADD_VALUE_MINUS_ONE, 0};
    int status = semop(semId, &operation, 1);
    if (status < 0)
        return status;
    so_debug("PID %d: DEPOIS da operação de adição de %d ao semáforo %d", getpid(), SEM_ADD_VALUE_MINUS_ONE, idxSem);
    return semGetValue(semId, idxSem);
}