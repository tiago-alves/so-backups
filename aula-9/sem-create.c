#include "defines.h"

#define NR_SEMS 3
#define ACCESS_PERMS 0600

int main() 
{
    int id = semget(IPC_KEY, NR_SEMS, IPC_CREAT | IPC_EXCL | ACCESS_PERMS);
    so_exit_on_error(id, "Erro semget Create");
    so_debug("Estou a usar o grupo de semáforos com key=0x%x e id=%d", IPC_KEY, id);

}