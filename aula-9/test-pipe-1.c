#include "defines.h"

int main() {
    int fd[2];
    pipe(fd);
    int pidFilho = fork();
    so_exit_on_error(pidFilho, "Erro no Fork");

    if (pidFilho == 0) { // Este é o processo filho
        close( fd[0] );
        FILE *saida = fdopen ( fd[1] , "w" );
        so_exit_on_null(saida, "Erro no fdopen saida");
        for ( int i = 0; i < 5; i++ ) {
            fprintf (saida, "Bom dia caro pai, tudo bem ?\n" );
        }
        fclose( saida );
        printf ("Fim do processo filho\n");
        exit(0);
    } else { // Este é o processo pai
        close ( fd[1] );
        char s[100];
        int i = 0;
        FILE *entrada = fdopen ( fd[0], "r" );
        so_exit_on_null(entrada, "Erro no fdopen entrada");
        while ( so_fgets( s, 100, entrada ) ) {
            printf ( "mensagem %d: %s", i++, s );
        }
        printf ("Fim do processo pai\n");
    }
}