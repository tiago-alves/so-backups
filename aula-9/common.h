#ifndef __COMMON_H__
    #define __COMMON_H__

    #include "/home/so/utils/include/so_utils.h"
    #include <sys/ipc.h>
    #include <sys/sem.h>
    #include <unistd.h>

    /**
     * Prototypes
     */
    int semCreate (int);
    int semGet ();
    int semRemove (int);
    int semSetValue (int, int, int);
    int semGetValue (int, int);
    int semUp (int, int);
    int semDown (int, int);
    int semAddValue (int, int, int);
#endif // __COMMON_H__