#include "defines.h"
/**
 * @brief Returns the value of the Semaphore idxSem (of the semaphore array defined by IPC_KEY)
 * @param semId id of the semaphore array defined by IPC_KEY
 * @param idxSem index of the semaphore in the semaphore array defined by IPC_KEY
 * @return the value of the semaphore, or -1 in case of error
 */
int semGetValue (int semId, int idxSem) {
    int semValue = semctl(semId, idxSem, GETVAL);
    if (semValue >= 0)
        so_debug("O valor do semáforo %d é %d", idxSem, semValue);
    return semValue;
}