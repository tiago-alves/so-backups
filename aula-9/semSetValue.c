#include "defines.h"
/**
 * @brief Sets the value of the Semaphore idxSem to value semValue (of the semaphore array defined by IPC_KEY)
 * @param semId id of the semaphore array defined by IPC_KEY
 * @param idxSem index of the semaphore in the semaphore array defined by IPC_KEY
 * @param semValue value to be set on the semaphore
 * @return semValue, or -1 in case of error
 */
int semSetValue (int semId, int idxSem, int semValue) {
    int status = semctl(semId, idxSem, SETVAL, semValue);
    if (status < 0)
        return status;
    so_debug("O novo valor do semáforo %d é %d", idxSem, semValue);
    return semValue;
}