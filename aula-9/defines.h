#ifndef __DEFINES_H__
    #define __DEFINES_H__

    #include "common.h"

    /**
     * KEY to be used on all Linux IPC get operations
     */
    #define IPC_KEY 0x0a106090 
    
#endif // __DEFINES_H__