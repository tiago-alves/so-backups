#include "defines.h"
/**
 * @brief Gets (retrieves) an already created semaphore array defined by IPC_KEY.
 * @param nrSems number of semaphores in the semaphore array
 * @return the corresponding semId of the semaphore array, or -1 in case of error
 */
int semGet () {
    #define DONT_CARE 0
    #define IPC_GET 0
    int semId = semget(IPC_KEY, DONT_CARE, IPC_GET);
    if (semId > 0) {
        so_debug("Estou a usar o grupo de semáforos com key=0x%x e semId=%d", IPC_KEY, semId);
    }
    return semId;
}