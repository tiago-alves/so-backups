#include "defines.h"
#include <sys/wait.h>

#define INPUT_END 0
#define OUTPUT_END 1

int main() {
    int fd[2];
    pipe(fd);

    int pid_filho = fork();
    so_exit_on_error(pid_filho, "Erro no primeiro Fork");

    if (!pid_filho) { // Código do Primeiro Filho
        dup2(fd[OUTPUT_END], STDOUT_FILENO);
        close(fd[INPUT_END]);
        close(fd[OUTPUT_END]);
        execl("/bin/cat", "cat", "/etc/passwd", NULL);
        perror("Failed to execute '/bin/cat'");
        exit(1);
    } else { // Código do Pai
        pid_filho = fork();
        so_exit_on_error(pid_filho, "Erro no segundo Fork");
        if (!pid_filho) { // Código do Segundo Filho
            dup2(fd[INPUT_END], STDIN_FILENO);
            close(fd[INPUT_END]);
            close(fd[OUTPUT_END]);
            execl("/bin/grep", "grep", "root", NULL);
            perror("Failed to execute '/bin/grep'");
            exit(1);
        } else { // Código do Pai
            close(fd[INPUT_END]);
            close(fd[OUTPUT_END]);
            wait(NULL);
        }
    }
}