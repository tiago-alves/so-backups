#include "defines.h"
/**
 * @brief Removes the semaphore array defined by IPC_KEY
 * @param semId id of the semaphore array defined by IPC_KEY
 * @return 0 in case of success, or -1 in case of error
 */
int semRemove (int semId) {
    #define DONT_CARE 0
    int status = semctl(semId, DONT_CARE, IPC_RMID);
    if (!status) {
        so_debug("Removi o grupo de semáforos com key=0x%x e semId=%d", IPC_KEY, semId);
    }
    return status;
}