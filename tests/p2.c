#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <wait.h>

#define SEM_X   0
#define SEM_Y   1

void downSem(int semId, int semN)
{

    struct sembuf DOWN =
    {
        .sem_num = semN,
        .sem_op = -1,
        .sem_flg = 0
    };

    semop(semId, &DOWN, 1);
}

void upSem(int semId, int semN)
{

    struct sembuf UP =
    {
        .sem_num = semN,
        .sem_op = 1,
        .sem_flg = 0
    };  // signal operation

    semop(semId, &UP, 1);
}

int main()
{
    int semId = semget(0xDEADBEEF, 2, 0);

    if(semId == -1)
        semId = semget(0xDEADBEEF, 2, IPC_CREAT | 0666);

    while(1)
    {
        downSem(semId, SEM_X);
        downSem(semId, SEM_X);
        printf("A");
        upSem(semId, SEM_Y);
        upSem(semId, SEM_Y);
    }

}