#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <wait.h>

#define SEM_X   0
#define SEM_Y   1

void downSem(int semId, int semN)
{
    struct sembuf DOWN =
    {
        .sem_num = semN,
        .sem_op = -1,
        .sem_flg = 0
    };

    //printf("Setting sem %hu DOWN by %hi\n", DOWN.sem_num, DOWN.sem_op);
    semop(semId, &DOWN, 1);
}

void upSem(int semId, int semN)
{
    struct sembuf UP =
    {
        .sem_num = semN,
        .sem_op = 1,
        .sem_flg = 0
    };  // signal operation

    //printf("Setting sem %hu UP by %hi\n", UP.sem_num, UP.sem_op);
    semop(semId, &UP, 1);
}

int main()
{
    int semId = semget(0xDEADBEEF, 2, 0);

    if(semId == -1)
        semId = semget(0xDEADBEEF, 2, IPC_CREAT | 0666);

    printf("semid %i\n", semId);
    semctl(semId, SEM_X, SETVAL, 2);
    semctl(semId, SEM_Y, SETVAL, 0);

    int PROCESS = fork();

    while(1)
    {
        //getchar();
        if(!PROCESS)
        {
            downSem(semId, SEM_Y);
            printf("B");            //  Se, em vez de "B", for "B\n", a sequencia sera correta
            fflush(stdout);
            upSem(semId, SEM_X);
        }
        else
        {
            downSem(semId, SEM_X);
            downSem(semId, SEM_X);
            printf("A");           //  Se, em vez de "A", for "A\n", a sequencia sera correta
            fflush(stdout);
            upSem(semId, SEM_Y);
            upSem(semId, SEM_Y);
        }
        sleep(1);
    }

}