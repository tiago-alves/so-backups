#include "/home/so/utils/include/so_utils.h"
#include <unistd.h>
#include <sys/wait.h>

//#define CHILD_SLEEPS

int main () {
    printf("Esta aplicação dorme durante 20s:\n");
    printf("Primeiro sleep de 10s:\n");
    sleep(10);
    printf("Segundo sleep de 10s:\n");
#if defined (CHILD_SLEEPS)
	int pid = fork();
	
	if(!pid)
		execl("/bin/sleep", "sleep", "10", NULL);
    
	wait(NULL);
#else	
	execl("/bin/sleep", "sleep", "10", NULL);
#endif

	printf("Fim do comando: sleep 10\n");
}
