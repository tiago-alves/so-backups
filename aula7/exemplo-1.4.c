#include "/home/so/utils/include/so_utils.h"
#include <unistd.h>
#include <sys/wait.h>

int main (int argc, char *argv[]) {
    int pidFilho;
    if (argc < 3) {
        so_error("", "SYNTAX: %s <SLEEP PAI> <SLEEP FILHO>", argv[0]);
        exit(0);
    }
    printf("Olá pessoal, sou um processo a correr, o meu PID é %d, o PID do meu pai é %d\nTecle ENTER\n", getpid(), getppid());
    getchar(); // Espera que o utilizador pressione <ENTER>
    printf("Vou fazer um fork, e guardar o PID do meu filho\n");
    pidFilho = fork();
    so_exit_on_error(pidFilho, "Erro no fork");
    printf("PAI e/ou FILHO ?? Depois do fork, o valor da variável pidFilho é %d\n", pidFilho);
    if (!pidFilho) { // Código FILHO
        printf("FILHO: O valor da variável pidFilho é %d, o meu PID é %d, o PID do meu pai é %d\n", pidFilho, getpid(), getppid());
        int sleepFilho = atoi(argv[2]);
        sleep(sleepFilho);  // Dorme o tempo especificado no argumento <SLEEP FILHO>
        printf("FILHO: Saí do sleep\n");
    } else { // Código PAI
        printf("PAI: O valor da variável pidFilho é %d, o meu PID é %d, o PID do meu pai é %d\n", pidFilho, getpid(), getppid());
        int sleepPai = atoi(argv[1]);
        sleep(sleepPai);    // Dorme o tempo especificado no argumento <SLEEP PAI>
        printf("PAI: Saí do sleep\n");
        // wait(NULL);
    }
    printf("PAI|FILHO: Depois do \"if\", Sou novamente um processo a correr, o meu PID é %d, o PID do meu pai é %d\n", getpid(), getppid());
    printf("PAI|FILHO: Adeus\n");
}
