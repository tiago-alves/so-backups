#include "/home/so/utils/include/so_utils.h"
#include <unistd.h>
#include <signal.h>

int terminaPrograma = FALSE;

void trata_sinal (int sinal) {
	printf("Recebi sinal %d\n", sinal);
	terminaPrograma = TRUE;
}

int main () {
	signal(SIGALRM, trata_sinal);
	printf("esperar 5 segundos...\n");
	alarm(5);
	while (!terminaPrograma)
        /* do nothing */;
	printf("Terminei\n");
}