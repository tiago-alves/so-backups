#include <stdio.h>
#include <unistd.h>
#include "/home/so/utils/include/so_utils.h"

int main () {

	int pid;
	pid = fork();

	if( !pid )
	{
		printf("FILHO: Var pid: %i, PID:%d, PID pai:%d\n", pid, getpid(), getppid());
		sleep(5);
	}
	else
	{
		sleep(5);
		printf("PAI: Var pid: %i, PID:%d, PID pai:%d\n", pid, getpid(), getppid());
	}	
	
	printf("FORA DO IF: Var pid: %i, PID:%d, PID pai:%d\n", pid, getpid(), getppid());
}
