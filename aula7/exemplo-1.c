#include <stdio.h>
#include <unistd.h>
#include "/home/so/utils/include/so_utils.h"

int main () {
    printf("Início: PID:%d, PID pai:%d\n", getpid(), getppid());
    sleep(10);
    printf("Fim: PID:%d, PID pai:%d\n", getpid(), getppid());
}
