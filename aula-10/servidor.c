#include "defines.h"

#define MSG_LOGIN 1

typedef struct {
    long msgType;
    struct {
        char texto[80];
        char nome[20];
        int pidCliente;
    } msgData;
} MsgContent;

int main() {
    #define ACCESS_PERMS 0600
    int id = msgget(IPC_KEY, IPC_CREAT | ACCESS_PERMS);
    so_exit_on_error(id, "Erro msgget Create");
    so_debug("Estou a usar a fila de mensagens com key=0x%x e id=%d", IPC_KEY, id);

    MsgContent msg;
    while (TRUE) {
        so_exit_on_error(msgrcv(id, &msg, sizeof(msg.msgData), MSG_LOGIN, 0), "Erro msgrcv");
        so_debug("Recebi de %s (PID %d) a msg: %s", msg.msgData.nome, msg.msgData.pidCliente, msg.msgData.texto);

        msg.msgType = msg.msgData.pidCliente;
		snprintf(msg.msgData.texto, sizeof(msg.msgData), "Sr(a) %s recebi a sua msg", msg.msgData.nome);
        so_exit_on_error(msgsnd(id, &msg, sizeof(msg.msgData), 0), "Erro msgsnd");
        so_debug("Enviei resposta para o cliente %ld", msg.msgType);
    }
}
