#include "defines.h"

typedef struct 
{
    long msgType;
    struct 
	{
        char texto[80];
    } msgData;
} MsgContent;

int main() 
{
    #define IPC_GET 0
    int id = msgget(IPC_KEY, MSGTYPE_LOGIN);
    so_exit_on_error(id, "Erro msgget");

    MsgContent msg;

    int status = msgrcv(id, &msg, sizeof(msg.msgData), 33, 0);
    so_exit_on_error(status, "Erro msgrcv");
    so_debug("A mensagem foi recebida com o tipo %ld, tamanho %ld, e conteúdo: %s",
                            msg.msgType, sizeof(msg.msgData), msg.msgData.texto);
}
