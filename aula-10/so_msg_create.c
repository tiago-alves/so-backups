#include "defines.h"

#define ACCESS_PERMS 0600

int main() 
{
    int id = msgget(IPC_KEY, 0);

	if(id > 0)
	{
		if(msgctl(id, IPC_RMID, 0) == -1)
		{
			so_debug("Erro a tentar remover fila");
			exit(1);	
		}
	}
	id = msgget(IPC_KEY, IPC_CREAT | IPC_EXCL | ACCESS_PERMS);
    so_exit_on_error(id, "Erro msgget Create");
    //so_debug("Estou a usar a fila de mensagens com key=0x%x e id=%d", IPC_KEY, id);
    so_success("","Estou a usar a fila de mensagens com key=0x%x e id=%d", IPC_KEY, id);
}
