#include "defines.h"

typedef struct 
{
    long msgType;
    struct 
	{
        char texto[MSGDATA_MAXSIZE];
    } msgData;
} MsgContent;

int main(int argc, char *argv[]) 
{
    int id = msgget(IPC_KEY, 0);
    so_exit_on_error(id, "Erro msgget");

	so_debug("Estou a usar a fila de mensagens com key 0x%x e id=%d", IPC_KEY, id);

	if(argc < 3)
	{
		so_error("Argumentos insuficientes", "SINTAX: %s <msgType> <msgSize>", argv[0]);
		exit(-1);
	}

	int msgType = atoi(argv[1]);
	int msgSize = atoi(argv[2]);
	
	if(msgType <= 0 || msgSize <= 0 || msgSize > MSGDATA_MAXSIZE)	
	{
		so_error("Argumentos com valores invalidos", "msgType: %d, msgSize: %d", msgType, msgSize);
		exit(-1);
	}
    
	MsgContent msg;
    msg.msgType = msgType;
	
	snprintf(msg.msgData.texto, MSGDATA_MAXSIZE, "Mensagem com %d Bytes", msgSize);

    int status = msgsnd(id, &msg, msgSize, 0);
	so_debug("=================");
    so_exit_on_error(status, "Erro msgsnd");
	so_debug("=================");
    so_debug("A mensagem foi enviada com o tipo %d, tamanho %d, e conteúdo: %s", msg.msgType, msgSize, msg.msgData.texto);
}
