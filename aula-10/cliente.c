#include "defines.h"
#include <unistd.h>

#define ACCESS_PERMS 0600
#define MSG_LOGIN 1

typedef struct {
    long msgType;
    struct {
        char texto[80];
        char nome[20];
        int pidCliente;
    } msgData;
} MsgContent;

int main() 
{

    int id = msgget(IPC_KEY, IPC_CREAT | ACCESS_PERMS);
    so_exit_on_error(id, "Erro msgget Create");
    so_debug("Sou o processo Cliente com PID=%d", getpid());
    so_debug("Estou a usar a fila de mensagens com key=0x%x e id=%d", IPC_KEY, id);

    MsgContent msg;

    while (TRUE) 
	{
		msg.msgType = MSG_LOGIN;
		msg.msgData.pidCliente = getpid();
		
		snprintf(msg.msgData.nome, 20, "Cliente %d", getpid());
		printf("Diga a mensagem:");
		so_gets(msg.msgData.texto, 80);

		int status = msgsnd(id, &msg, sizeof(msg.msgData), 0);
		so_exit_on_error(status, "Erro msgsnd");
		so_debug("A mensagem foi enviada com o tipo %d, tamanho %d, e conteudo %s",
												msg.msgType, sizeof(msg.msgData), msg.msgData.texto);
		
		status = msgrcv(id, &msg, sizeof(msg.msgData), getpid(), 0);

		so_exit_on_error(status, "Erro msgrcv");
        so_debug("Recebi a msg resposta: %s", msg.msgData.texto);
    }
}
