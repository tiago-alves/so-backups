#include "defines.h"

typedef struct 
{
    long msgType;
    struct 
	{
        char texto[80];
    } msgData;
} MsgContent;

int main() 
{
    #define IPC_GET 0
    int id = msgget(IPC_KEY, IPC_GET);
    so_exit_on_error(id, "Erro msgget");

    MsgContent msg;
    msg.msgType = 33;
    printf("Escreva a mensagem: ");
    so_gets(msg.msgData.texto, sizeof(msg.msgData.texto));

    int status = msgsnd(id, &msg, sizeof(msg.msgData), 0);
    so_exit_on_error(status, "Erro msgsnd");
    so_debug("A mensagem foi enviada com o tipo %ld, tamanho %ld, e conteúdo: %s",
                            msg.msgType, sizeof(msg.msgData), msg.msgData.texto);
}
