#ifndef __COMMON_H__
    #define __COMMON_H__

    #include "/home/so/utils/include/so_utils.h"
    #include <sys/ipc.h>
    #include <sys/sem.h>
    #include <sys/msg.h>
    #include <sys/shm.h>
    #include <unistd.h>

    #define MSGDATA_MAXSIZE 8192    // Maximum size of msgData on Tigre

#endif // __COMMON_H__