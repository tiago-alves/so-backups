#!/bin/bash
# gerar numero aleatorio

n=$(($RANDOM%100))
echo $n
# numero de tentativas
i=1
echo "Adivinhe o numero entre 0 e 100 em que estou a pensar"
echo
while :
	do
		echo "Introduza a sua tentativa #$1 (0 para desistir):"
		read guess
		#if [[ $guess == $n || $guess == "0" ]]
		if ((guess==n||guess==0))
			then
				break
		fi
		#if [[ $guess < $n ]] # NOK
		if ((guess<n))
			then
				echo "muito pequeno"
			else
				echo "muito grande"
		fi
		i=$((i+1))
		#((i++))
done

if [[ $guess == "0" ]]
#if ((guess==0))	
	then
		echo "Nao adivinhou em $(($i-1)) tentativas, o numero era $n."
	else
		echo "Parabens, acertou em $i tentativas!"
fi
